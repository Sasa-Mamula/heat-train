using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Quests : MonoBehaviour
{
    public Message[] messages;
    public Npc[] npcs;
   public GameObject quest;
 

  
    public void TriggerDialogue()
    {

        FindObjectOfType<DialogeManager>().StartDialogue(messages, npcs);

    }

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            FindObjectOfType<DialogeManager>().StartDialogue(messages, npcs);
            quest.GetComponent<Canvas>().enabled = true;

        }

    }
    private void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            FindObjectOfType<DialogeManager>().EndDialogue();
            this.gameObject.GetComponent<BoxCollider>().enabled = false;
            quest.GetComponent<Canvas>().enabled = false;

        }
    }

   

}

[System.Serializable]
public class MessageQ
{
    public int npcId;
    [TextArea(3, 10)]
    public string message;
}

[System.Serializable]
public class NpcQ
{
    public string npcName;
    public Sprite sprite;
}
