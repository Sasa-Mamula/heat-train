using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class endScript : MonoBehaviour
{
    public VideoPlayer endWin;
    public GameObject resources;
    public Slider Satisfaction;
    public GameObject Waypoint;
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            StartCoroutine(show());
        }
    }

    IEnumerator show()
    {
        yield return new WaitForSeconds(2);
        endWin.Play();
        resources.SetActive(false);
        Satisfaction.gameObject.SetActive(false);
        Waypoint.gameObject.SetActive(false);
        yield return new WaitForSeconds(40);
        SceneManager.LoadScene("MainMenu");
    }

    
}
