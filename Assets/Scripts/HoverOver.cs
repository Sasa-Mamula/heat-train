using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class HoverOver : MonoBehaviour 
{
    public GameObject[] btnCheck;

   
    public void Open(GameObject gm)
    {
        foreach ( GameObject btn in btnCheck)
        {
            if (btn.gameObject.activeInHierarchy)
            {
                btn.SetActive(false);
            }
        }
        gm.SetActive(true); 
    }
    
    



}
