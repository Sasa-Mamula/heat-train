using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour
{
    public Transform target;
    float dist;
    public float maxDist;
    public float projectSpeed;
    public Transform barrel;
    public GameObject projectile;
    public float fireRate, nextFire;
    public string enemyTag = "Enemy";
    public Transform head;
    public float turnSpeed = 10f;
    public GameObject FireEffect;
    AudioSource FireSound;
    GameObject nearestEnemy = null;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("UpdateTarget", 0f, 0.5f);
        FireSound = GetComponent<AudioSource>();
        

    }
    void UpdateTarget()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag(enemyTag);
        float shortestDistance = Mathf.Infinity;
        
        foreach (GameObject enemy in enemies)
        {
            dist = Vector3.Distance(transform.position, enemy.transform.position);
            if (dist < shortestDistance)
            {
                shortestDistance = dist;
                nearestEnemy = enemy;

            }
        }

        if (nearestEnemy != null && shortestDistance <= maxDist)
        {
            target = nearestEnemy.transform;
           
        }
        else
        {
            target = null;
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (target == null)
        {
            return;
        }
        
        
        if(nearestEnemy.GetComponent<EnemyDamage>().currentHealth > 0)
        {
            LockOnTarget();
            if (Time.time >= nextFire)
            {
                nextFire = Time.time + 2f / fireRate;
                shoot();


            }
        }
   
    }
    void LockOnTarget()
    {
        Vector3 dir = target.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(dir);
        Vector3 rotation = Quaternion.Lerp(head.rotation, lookRotation, Time.deltaTime * turnSpeed).eulerAngles;
        head.rotation = Quaternion.Euler(rotation.x, rotation.y, 0f);
    }

  
    void shoot()
    {
        GameObject Clone = Instantiate(projectile, barrel.position, transform.rotation);
        GameObject Effect = Instantiate(FireEffect, barrel.position, transform.rotation);
        //dodavanje pucanja tocnije izbacivanja
        Clone.GetComponent<Rigidbody>().AddForce(transform.forward *10 * projectSpeed);
        Destroy(Clone, 1f);
        FireSound.Play();

    }

    
    
}
