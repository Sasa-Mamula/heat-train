using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Popup : MonoBehaviour
{
    public Message[] messages;
    public Npc[] npcs;
    public TMP_Text message;
    public GameObject popup;
    public GameObject Resources;
    public GameObject Train;
    public int pay = 100;
    public int wait = 2;
    public Button button;
    public RectTransform backgroundBox;
    public AudioSource paySound;

    private void Start()
    {
        backgroundBox.transform.transform.localScale = Vector3.zero;
        Resources = GameObject.Find("Humanoid-GEN");
        message.text = "You need to pay for fixes please confirm!";
        Train = GameObject.FindGameObjectWithTag("Train");
        paySound = GetComponent<AudioSource>();
    }

   
    public void TriggerDialogue()
    {

        FindObjectOfType<DialogeManager>().StartDialogue(messages, npcs);

    }

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            FindObjectOfType<DialogeManager>().StartDialogue(messages, npcs);
            popup.SetActive(true);
            button.gameObject.SetActive(true);
            backgroundBox.LeanScale(Vector3.one, 0.5f).setEaseOutExpo();
        }

    }
    private void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            FindObjectOfType<DialogeManager>().EndDialogue();
            this.gameObject.GetComponent<BoxCollider>().enabled = false;
           


        }
    }

    public void PayToRepair()
    {
        Resources.GetComponent<PlayerStats>().metal -= pay;
        popup.SetActive(false);
        button.gameObject.SetActive(false);
        backgroundBox.LeanScale(Vector3.zero, 0.5f).setEaseOutExpo();
        Train.GetComponent<Follower>().TakeSatisfy(2);
        paySound.Play();
    }

   

}

[System.Serializable]
public class MessageD
{
    public int npcId;
    [TextArea(3, 10)]
    public string message;
}

[System.Serializable]
public class NpcS
{
    public string npcName;
    public Sprite sprite;
}
