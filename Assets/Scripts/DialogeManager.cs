using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class DialogeManager : MonoBehaviour
{
	public TMP_Text nameText;
	public TMP_Text messageText;
	public Image npcImage;
	public TMP_Text continueText;
	AudioSource popUp;
	public RectTransform backgroundBox;
	Message[] currentMessages;
	Npc[] currentNpc;
	int activeMessage = 0;
	public static bool isActive = false;

	void Start()
	{
		backgroundBox.transform.transform.localScale = Vector3.zero;
		popUp = GetComponent<AudioSource>();
		
	}

	public void StartDialogue(Message[] messages, Npc[] npcs)
	{
		currentMessages = messages;
		currentNpc = npcs;
		activeMessage = 0;
		isActive = true;
	
		
		DisplayMessage();
		backgroundBox.LeanScale(Vector3.one, 0.5f).setEaseOutExpo();
		popUp.Play();
		
	}

	void DisplayMessage()
    {
		Message messageToDisplay = currentMessages[activeMessage];
		messageText.text = messageToDisplay.message;

		Npc npcToDisplay = currentNpc[messageToDisplay.npcId];
		nameText.text = npcToDisplay.npcName;
		npcImage.sprite = npcToDisplay.sprite;
		AnimateTextColor();
		
	}

	public void nextMessage()
	{
		activeMessage++;
		if (activeMessage < currentMessages.Length)
        {
			DisplayMessage();
        }
        else
        {
			EndDialogue();
		}

	}

	public void EndDialogue()
    {
		
		isActive = false;
		backgroundBox.LeanScale(Vector3.zero, 0.5f).setEaseOutExpo();
		popUp.Play();
		

	}
	void AnimateTextColor()
    {
		LeanTween.textAlpha(messageText.rectTransform, 0, 0);
		LeanTween.textAlpha(messageText.rectTransform, 1, 0.5f);
		LeanTween.textAlpha(continueText.rectTransform, 0, 0);
		LeanTween.textAlpha(continueText.rectTransform, 1, 0.5f);
	}
	// Use this for initialization
	

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && isActive == true)
        {
			nextMessage();
        }
    }


}
