using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BacgroundAudio : MonoBehaviour
{
    AudioSource backgroundMusic;
    private void Start()
    {
        backgroundMusic = GetComponent<AudioSource>();
    }
    private void Awake()
    {
        StartCoroutine(StartMusic());
    }
    IEnumerator StartMusic()
    {
        yield return new WaitForSeconds(84);
        backgroundMusic.Play();
        DontDestroyOnLoad(backgroundMusic);
    }
}
