using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameManager : MonoBehaviour
{
    public GameObject PipesHolder;
    public GameObject[] Pipes;

    [SerializeField]
    public int totalPipes = 0;
    [SerializeField]
    public int correctedPipes = 0;

    public TMP_Text WinText;
    public TMP_Text Timer;
    
    public GameObject[] PipesCanvas;

    // Start is called before the first frame update
    void Start()
    {
        Timer.gameObject.SetActive(true);
        WinText.gameObject.SetActive(false);
        
        totalPipes = PipesHolder.transform.childCount;

        Pipes = new GameObject[totalPipes];

        for (int i = 0; i < Pipes.Length; i++)
        {
            Pipes[i] = PipesHolder.transform.GetChild(i).gameObject;
        }

        
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            PipesCanvas[0].GetComponent<Canvas>().enabled = false;
            PipesCanvas[1].GetComponent<Canvas>().enabled = false;
            PipesCanvas[2].GetComponent<Canvas>().enabled = false;
        }
    }

    public void correctMove()
    {
        correctedPipes += 1;
        

        

        if (correctedPipes == totalPipes)
        {
           
            WinText.gameObject.SetActive(true);
        }
       

    }

    public void wrongMove()
    {
        
        correctedPipes -= 1;
        
    }

   
}
