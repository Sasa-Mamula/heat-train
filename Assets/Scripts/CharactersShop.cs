using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CharactersShop : MonoBehaviour
{
    public int ItemID;
    public TMP_Text Price;
    public GameObject CharacterManager;

    private void Update()
    {
        Price.text = "Price: " + CharacterManager.GetComponent<CharactersManager>().CharItems[2, ItemID].ToString();
    }
}
