using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Healthbar : MonoBehaviour
{
    public Slider Health;
    
    public void SetMaXHealth(int health)
    {
        Health.maxValue = health;
        Health.value = health;
    }
    public void setHealth(int health)
    {
        Health.value = health;    
    }
}
