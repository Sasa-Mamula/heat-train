﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed;
    public float rotationSpeed;
    Animator genAnim;
    private Rigidbody rb;
    private void Start()
    {
        genAnim = GetComponent<Animator>();
        
        Rigidbody rb = GetComponent<Rigidbody>();
    }
    void Update()
    {

        var horizontal = Input.GetAxis("Horizontal");
        var vertical = Input.GetAxis("Vertical");
        genAnim.SetBool("walk", false);
        


        Vector3 movementDirection = new Vector3(horizontal, 0, vertical);
        movementDirection.Normalize();

        transform.Translate(movementDirection * speed * Time.deltaTime, Space.World);

        
        if (movementDirection != Vector3.zero )
        {
            Quaternion toRotation = Quaternion.LookRotation(movementDirection,Vector3.up);
            transform.rotation = Quaternion.RotateTowards(transform.rotation,toRotation, rotationSpeed * Time.deltaTime);
            genAnim.SetBool("walk", true);
            
        } 
        

    }

      

}
