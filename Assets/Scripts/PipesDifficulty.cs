using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class PipesDifficulty : MonoBehaviour
{
    public GameObject[] lvl;
    public GameObject Resources;
    public Slider Plvl;
    public GameObject TurnOffMinigame;
    public TMP_Text valueT;
    public TMP_Text minT;
    public TMP_Text maxT;
    public TMP_Text confirm;
    int minS = 0;
    int maxS = 5000;
    public float TimeLeft1;
    public float TimeLeft2;
    public float TimeLeft3;
    public bool TimerOn = false;
    public TMP_Text[] TimerTxt;
    public GameObject Train;
    public GameObject removeCanvas;
    public GameObject gameManager;
    AudioSource paySound;
   

    private void Start()
    {
        Plvl.minValue = minS;
        Plvl.maxValue = maxS;
        minT.text = minS.ToString("F0");
        maxT.text = maxS.ToString("F0");
        Train = GameObject.FindGameObjectWithTag("Train");
        Resources = GameObject.Find("Humanoid-GEN");
        paySound = GetComponent<AudioSource>();
    }
    private void Update()
    {
        valueT.text = Plvl.value.ToString("F0");
        confirm.text = Plvl.value.ToString("F0");
        if (Plvl.value <= 1000)
        {
            TimerSetting1();
        }
        if (Plvl.value >= 1001 && Plvl.value <= 3000)
        {
            TimerSetting2();
        }
        if (Plvl.value >= 3001 && Plvl.value <= 5000)
        {
            TimerSetting3();
        }
        
    }


    public void WhatLevel()
    {
        if (Plvl.value <= 1000)
        {
            lvl[0].GetComponent<Canvas>().enabled = true;
            Resources.GetComponent<PlayerStats>().metal -= Plvl.value;
            TimerOn = true;
            TimerSetting1();
            removeCanvas.GetComponent<Canvas>().enabled = false;
            paySound.Play();
            
        }
        if (Plvl.value >= 1001 && Plvl.value <= 3000)
        {
            lvl[1].GetComponent<Canvas>().enabled = true;
            Resources.GetComponent<PlayerStats>().metal -= Plvl.value;
            Train.GetComponent<Follower>().TakeSatisfy(10);
            TimerOn = true;
            TimerSetting2();
            removeCanvas.GetComponent<Canvas>().enabled = false;
            paySound.Play();
        }
        if (Plvl.value >= 3001 && Plvl.value <= 5000)
        {
            lvl[2].GetComponent<Canvas>().enabled = true;
            Resources.GetComponent<PlayerStats>().metal -= Plvl.value;
            Resources.GetComponent<PlayerStats>().water -= 2000;
            Resources.GetComponent<PlayerStats>().oxygen -= 0.05f;
            Resources.GetComponent<PlayerStats>().food -= 500;
            Train.GetComponent<Follower>().TakeSatisfy(15);
            TimerOn = true;
            TimerSetting3();
            paySound.Play();
            removeCanvas.GetComponent<Canvas>().enabled = false;
        }
        Plvl.GetComponent<Slider>().interactable = false;



    }

   void TimerSetting1()
    {
        if (TimerOn)
        {
            if (TimeLeft1 > 0)
            {
                if (gameManager.GetComponent<GameManager>().correctedPipes == gameManager.GetComponent<GameManager>().totalPipes) 
                { 
                    TimerOn = false;
                    Train.GetComponent<Follower>().GiveSatisfy(1.5f);
                }
                TimeLeft1 -= Time.deltaTime;
                updateTimer(TimeLeft1);
            }
            else
            {
                TimerTxt[0].text = "Time is Up";
                TimeLeft1 = 0;
                TimerOn = false;
                Train.GetComponent<Follower>().TakeSatisfy(3);
            }
        }       
    }

    void TimerSetting2()
    {
        if (TimerOn)
        {
            if (TimeLeft2 > 0)
            {
                if (gameManager.GetComponent<GameManager2>().correctedPipes == gameManager.GetComponent<GameManager2>().totalPipes)
                {
                    TimerOn = false;
                    Train.GetComponent<Follower>().GiveSatisfy(5);
                }
                TimeLeft2 -= Time.deltaTime;
                updateTimer(TimeLeft2);
            }
            else
            {
                TimerTxt[1].text = "Time is Up";
                TimeLeft2 = 0;
                TimerOn = false;
                Train.GetComponent<Follower>().TakeSatisfy(10);
            }
        }
    }

    void TimerSetting3()
    {
        if (TimerOn)
        {
            if (TimeLeft3 > 0)
            {
                if (gameManager.GetComponent<GameManager3>().correctedPipes == gameManager.GetComponent<GameManager3>().totalPipes)
                {
                    TimerOn = false;
                    Train.GetComponent<Follower>().GiveSatisfy(10);
                }
                TimeLeft3 -= Time.deltaTime;
                updateTimer(TimeLeft3);
            }
            else
            {
                TimerTxt[2].text = "Time is Up";
                TimeLeft3 = 0;
                TimerOn = false;
                Train.GetComponent<Follower>().TakeSatisfy(15);
            }
        }
    }

    public void updateTimer(float currentTime)
    {
        currentTime += 1;

        float minutes = Mathf.FloorToInt(currentTime / 60);
        float seconds = Mathf.FloorToInt(currentTime % 60);

        TimerTxt[0].text = string.Format("Time left: {0:00}:{1:00}", minutes, seconds);
        TimerTxt[1].text = string.Format("Time left: {0:00}:{1:00}", minutes, seconds);
        TimerTxt[2].text = string.Format("Time left: {0:00}:{1:00}", minutes, seconds);

       
    }

    
}
