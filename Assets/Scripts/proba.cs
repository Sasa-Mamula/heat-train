using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class proba : MonoBehaviour
{
    GameObject gm;
   
    private void Start()
    {
        gm = GameObject.Find("Train");
      
    }
    
    
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            gameObject.GetComponent<BoxCollider>().enabled = false;
            gameObject.GetComponent<Target>().enabled = false;
            gm.GetComponent<PlaceManager>().Places(this.gameObject.transform);
            
        }


    }
}
