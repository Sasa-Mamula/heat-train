using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;
using TMPro;

public class Follower : MonoBehaviour
{
    public Text HText;
    public Slider Health;
    public Slider Satisfaction;
    public Text Stext;
    public Slider HealthBar;
    public GameObject gameOverScreen;
    public GameObject PouseScreen;
    public TMP_Text gmText;
    public VideoPlayer intro;
    bool pouse;
    public GameObject Waypoint;
    public GameObject resources;
    public GameObject tutorial;
  
   

    private void Start()
    {
        intro.Play();
        HealthBar.gameObject.SetActive(false);
        Satisfaction.gameObject.SetActive(false);
        Waypoint.gameObject.SetActive(false);
        resources.gameObject.SetActive(false);
        tutorial.gameObject.SetActive(false);
        StartCoroutine(HideGUI());
    }
    void Update()
    {
       

        HText.text = Health.value.ToString("F0");

        Stext.text = Satisfaction.value.ToString();
        Pouse();
        GameOver();
        
    }
   

    public void TakeDamage(float damage)
    {
        HealthBar.value -= damage;
        
    }

    public void TakeSatisfy(float satisfy)
    {
        Satisfaction.value -= satisfy;
    }
    public void GiveSatisfy(float satisfy)
    {
        Satisfaction.value += satisfy;
    }

    public void GameOver()
    {
        if (HealthBar.value <= 0 )
        {
            gameOverScreen.gameObject.SetActive(true);
            GameObject.FindGameObjectWithTag("Spawner").GetComponent<WaveSpawner>().enabled = false;
            Time.timeScale = 0;
            gmText.text = "Your train didn't survive!";
        }
        if (Satisfaction.value <= 20)
        {
            gmText.text = "Your people weren't satisfyed enough and they killed you!";
            Time.timeScale = 0;
            gameOverScreen.gameObject.SetActive(true);
        }
      
    }

    public void Pouse()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            pouse = !pouse;
            
            
        }
        if (pouse)
        {
            PouseScreen.gameObject.SetActive(true);
            Time.timeScale = 0;
        }
        else
        {
            PouseScreen.gameObject.SetActive(false);
            Time.timeScale = 1;
        }
    }

    IEnumerator HideGUI()
    {
        yield return new WaitForSeconds(1);
        Satisfaction.gameObject.SetActive(true);
        Waypoint.SetActive(true);
        resources.SetActive(true);
        tutorial.SetActive(true);
        intro.Stop();
    }

    

}
