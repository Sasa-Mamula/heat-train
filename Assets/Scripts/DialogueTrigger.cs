using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTrigger : MonoBehaviour
{
    public Message[] messages;
    public Npc[] npcs;
   
    public void TriggerDialogue()
	{
        
		FindObjectOfType<DialogeManager>().StartDialogue(messages, npcs);
		
	}

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            FindObjectOfType<DialogeManager>().StartDialogue(messages, npcs);
        }

    }
    private void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            FindObjectOfType<DialogeManager>().EndDialogue();
            this.gameObject.GetComponent<BoxCollider>().enabled = false;
            


        }
    }

   
}

[System.Serializable]
public class Message
{
    public int npcId;
    [TextArea(3, 10)]
    public string message;
}

[System.Serializable]
public class Npc
{
    public string npcName;
    public Sprite sprite;
}
