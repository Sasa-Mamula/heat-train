using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class ShopManager : MonoBehaviour
{
    public int[,] shopItems = new int[4, 4];
    public float coins;
    public TMP_Text CoinsTXT;
    public GameObject res;
    public GameObject turret;
    private GameObject upgradeTurret;
    private GameObject upgradeButton;
    private GameObject Train;
    AudioSource coinsSound;
    public TMP_Text fireRateText;
    public TMP_Text projectSpeedText;



    // Start is called before the first frame update
    void Start()
    {
        coinsSound = GetComponent<AudioSource>();
        upgradeTurret = GameObject.FindWithTag("Turret");
        upgradeButton = GameObject.FindWithTag("Upgrade");
        res = GameObject.Find("Humanoid-GEN");
        CoinsTXT.text = "Resources: " + coins.ToString();
        Train = GameObject.Find("Train");

          

        //Id
        shopItems[1, 1] = 1;
       

        //Price
        shopItems[2, 1] = 1000;
        
    }

    // Update is called once per frame
    private void Update()
    {

        CoinsTXT.text = "Resources: " + res.GetComponent<PlayerStats>().metal.ToString("F0");
        fireRateText.text = "Fire rate: " + upgradeTurret.GetComponent<Turret>().fireRate.ToString();
        projectSpeedText.text = "Prejectile speed: " + upgradeTurret.GetComponent<Turret>().projectSpeed.ToString();
    }
    public void Upgrade()
    {
        GameObject ButtonRef = GameObject.FindGameObjectWithTag("Event").GetComponent<EventSystem>().currentSelectedGameObject;



        if (res.GetComponent<PlayerStats>().metal >= shopItems[2, ButtonRef.GetComponent<UpgradeButton>().ItemID])
        {
            if (shopItems[1, ButtonRef.GetComponent<UpgradeButton>().ItemID] == 1)
            {

                upgradeTurret.GetComponent<Turret>().fireRate += 2;
                upgradeTurret.GetComponent<Turret>().projectSpeed += 15;
                coinsSound.Play();
               

                if (upgradeTurret.GetComponent<Turret>().fireRate == 7)
                {
                    upgradeButton.GetComponent<UpgradeButton>().MaxLvl.text = "Level 2";
                    shopItems[2, 1] = 2000;
                    coinsSound.Play();
                }
                if (upgradeTurret.GetComponent<Turret>().fireRate == 9)
                {
                    res.GetComponent<PlayerStats>().water -= 2000;
                    res.GetComponent<PlayerStats>().oxygen -= 0.05f;
                    Train.GetComponent<Follower>().TakeSatisfy(5);
                    shopItems[2, 1] = 3000;
                    coinsSound.Play();
                    upgradeButton.GetComponent<UpgradeButton>().MaxLvl.text = "Max Level";
                    ButtonRef.GetComponent<Button>().interactable = false;
                }

                res.GetComponent<PlayerStats>().metal -= shopItems[2, ButtonRef.GetComponent<UpgradeButton>().ItemID];
                
                CoinsTXT.text = "Resources: " + res.GetComponent<PlayerStats>().metal.ToString();

            }

            }
        
    }
}