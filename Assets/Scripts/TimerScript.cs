using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TimerScript : MonoBehaviour
{
    public float TimeLeft;
    public bool TimerOn = false;

    public TMP_Text TimerTxt;
    
    void Start()
    {
        TimerOn = true;
    }

    void Update()
    {
        updateTimer(TimeLeft);
    }
  

    public void updateTimer(float currentTime)
    {
        currentTime += 1;

        float minutes = Mathf.FloorToInt(currentTime / 60);
        float seconds = Mathf.FloorToInt(currentTime % 60);

        TimerTxt.text = string.Format("Time left: {0:00}:{1:00}", minutes, seconds);

        if (TimerOn)
        {
            if (TimeLeft > 0)
            {
                TimeLeft -= Time.deltaTime;
                updateTimer(TimeLeft);
            }
            else
            {
                TimerTxt.text = "Time is Up";
                TimeLeft = 0;
                TimerOn = false;

            }
        }
    }

   

}