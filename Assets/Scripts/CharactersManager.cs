using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;


public class CharactersManager : MonoBehaviour
{
    public int[,] CharItems = new int[10, 10];
    public float coins;
    public TMP_Text CoinsTXT;
    public GameObject res;
    AudioSource coinsSound;
    public GameObject wellDone;
    public TMP_Text message;
    public GameObject CharachterShop;
    GameObject Train;
    void Start()
    {
        CharachterShop = GameObject.Find("Characters Shop");
        coinsSound = GetComponent<AudioSource>();
        res = GameObject.Find("Humanoid-GEN");
        CoinsTXT.text = "Resources: " + coins.ToString();
        Train = GameObject.FindGameObjectWithTag("Train");

        //ID
        CharItems[1, 1] = 1;
        CharItems[1, 2] = 2;
        CharItems[1, 3] = 3;
        CharItems[1, 4] = 4;
        CharItems[1, 5] = 5;
        CharItems[1, 6] = 6;
        CharItems[1, 7] = 7;
        CharItems[1, 8] = 8;
        CharItems[1, 9] = 9;

        //Price tutorial
        CharItems[2, 1] = 1500;
        CharItems[2, 2] = 1000;


        //Price quest 1 
        CharItems[2, 3] = 2000;
        CharItems[2, 4] = 2500;
      
        //Price quest 2
        CharItems[2, 5] = 3500;
        CharItems[2, 6] = 3000;
        
        //End game
        CharItems[2, 7] = 4000;
        CharItems[2, 8] = 4500;
        CharItems[2, 9] = 4000;
    }


    void Update()
    {
        CoinsTXT.text = "Resources: " + res.GetComponent<PlayerStats>().metal.ToString();
    }

    public void Buy()
    {
        GameObject ButtonRef = GameObject.FindGameObjectWithTag("Event").GetComponent<EventSystem>().currentSelectedGameObject;



        if (res.GetComponent<PlayerStats>().metal >= CharItems[2, ButtonRef.GetComponent<CharactersShop>().ItemID])
        {
            //Tutorial
            if (CharItems[1, ButtonRef.GetComponent<CharactersShop>().ItemID] == 1)
            {

                res.GetComponent<PlayerStats>().metal -= CharItems[2, ButtonRef.GetComponent<CharactersShop>().ItemID];
                CoinsTXT.text = "Resources: " + res.GetComponent<PlayerStats>().metal.ToString();
                coinsSound.Play();
                message.text = "Well done Daniel did it for you";
                StartCoroutine(ShowHide());
                ButtonRef.GetComponent<CharactersShop>().GetComponent<Button>().interactable = false;
                Train.GetComponent<Follower>().GiveSatisfy(1);

            }

            if (CharItems[1, ButtonRef.GetComponent<CharactersShop>().ItemID] == 2)
            {

                res.GetComponent<PlayerStats>().metal -= CharItems[2, ButtonRef.GetComponent<CharactersShop>().ItemID];
                CoinsTXT.text = "Resources: " + res.GetComponent<PlayerStats>().metal.ToString();
                coinsSound.Play();
               
                message.text = "Well done Fabian did it for you";
                StartCoroutine(ShowHide());
                ButtonRef.GetComponent<CharactersShop>().GetComponent<Button>().interactable = false;
                Train.GetComponent<Follower>().GiveSatisfy(1);
            }

            //Quest 1
            if (CharItems[1, ButtonRef.GetComponent<CharactersShop>().ItemID] == 3)
            {

                res.GetComponent<PlayerStats>().metal -= CharItems[2, ButtonRef.GetComponent<CharactersShop>().ItemID];
                CoinsTXT.text = "Resources: " + res.GetComponent<PlayerStats>().metal.ToString();
                coinsSound.Play();

                message.text = "Well done Rita did it for you";
                StartCoroutine(ShowHide());
                ButtonRef.GetComponent<CharactersShop>().GetComponent<Button>().interactable = false;
                Train.GetComponent<Follower>().GiveSatisfy(1);
            }
            if (CharItems[1, ButtonRef.GetComponent<CharactersShop>().ItemID] == 4)
            {

                res.GetComponent<PlayerStats>().metal -= CharItems[2, ButtonRef.GetComponent<CharactersShop>().ItemID];
                CoinsTXT.text = "Resources: " + res.GetComponent<PlayerStats>().metal.ToString();
                coinsSound.Play();

                message.text = "Well done Helena did it for you";
                StartCoroutine(ShowHide());
                ButtonRef.GetComponent<CharactersShop>().GetComponent<Button>().interactable = false;
                Train.GetComponent<Follower>().GiveSatisfy(1);
            }

            if (CharItems[1, ButtonRef.GetComponent<CharactersShop>().ItemID] == 5)
            {

                res.GetComponent<PlayerStats>().metal -= CharItems[2, ButtonRef.GetComponent<CharactersShop>().ItemID];
                CoinsTXT.text = "Resources: " + res.GetComponent<PlayerStats>().metal.ToString();
                coinsSound.Play();

                message.text = "Well done Ben did it for you";
                StartCoroutine(ShowHide());
                ButtonRef.GetComponent<CharactersShop>().GetComponent<Button>().interactable = false;
                Train.GetComponent<Follower>().GiveSatisfy(1);
            }
            //Quest 2
            if (CharItems[1, ButtonRef.GetComponent<CharactersShop>().ItemID] == 6)
            {

                res.GetComponent<PlayerStats>().metal -= CharItems[2, ButtonRef.GetComponent<CharactersShop>().ItemID];
                CoinsTXT.text = "Resources: " + res.GetComponent<PlayerStats>().metal.ToString();
                coinsSound.Play();

                message.text = "Well done Tom did it for you";
                StartCoroutine(ShowHide());
                ButtonRef.GetComponent<CharactersShop>().GetComponent<Button>().interactable = false;
                Train.GetComponent<Follower>().GiveSatisfy(1);
            }

            //End game
            if (CharItems[1, ButtonRef.GetComponent<CharactersShop>().ItemID] == 7)
            {

                res.GetComponent<PlayerStats>().metal -= CharItems[2, ButtonRef.GetComponent<CharactersShop>().ItemID];
                CoinsTXT.text = "Resources: " + res.GetComponent<PlayerStats>().metal.ToString();
                coinsSound.Play();

                message.text = "Well done you choose the best trio for the job!";
                StartCoroutine(ShowHide());
                ButtonRef.GetComponent<CharactersShop>().GetComponent<Button>().interactable = false;
                Train.GetComponent<Follower>().GiveSatisfy(1);
            }

            if (CharItems[1, ButtonRef.GetComponent<CharactersShop>().ItemID] == 8)
            {

                res.GetComponent<PlayerStats>().metal -= CharItems[2, ButtonRef.GetComponent<CharactersShop>().ItemID];
                CoinsTXT.text = "Resources: " + res.GetComponent<PlayerStats>().metal.ToString();
                coinsSound.Play();

                message.text = "Well done you choose the best trio for the job!";
                StartCoroutine(ShowHide());
                ButtonRef.GetComponent<CharactersShop>().GetComponent<Button>().interactable = false;
                Train.GetComponent<Follower>().GiveSatisfy(1);
            }

            if (CharItems[1, ButtonRef.GetComponent<CharactersShop>().ItemID] == 9)
            {

                res.GetComponent<PlayerStats>().metal -= CharItems[2, ButtonRef.GetComponent<CharactersShop>().ItemID];
                CoinsTXT.text = "Resources: " + res.GetComponent<PlayerStats>().metal.ToString();
                coinsSound.Play();

                message.text = "Well done you choose the best trio for the job!";
                StartCoroutine(ShowHide());
                ButtonRef.GetComponent<CharactersShop>().GetComponent<Button>().interactable = false;
                Train.GetComponent<Follower>().GiveSatisfy(1);
            }
        }
    }
    IEnumerator ShowHide()
    {
        wellDone.SetActive(true);
        yield return new WaitForSeconds(2);
        wellDone.SetActive(false);
    }

   
}
 