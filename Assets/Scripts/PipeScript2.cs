using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeScript2 : MonoBehaviour
{
    float[] rotations = { 0, 90, 180, 270 };

    public float[] correctRotation;
    [SerializeField]
    bool isPlaced = false;

    int PossibleRots = 1;

    public GameObject gameManager;

    private void Awake()
    {
        gameManager.GetComponent<GameManager2>();
    }

    private void Start()
    {
        PossibleRots = correctRotation.Length;
        int rand = Random.Range(0, rotations.Length);
        transform.eulerAngles = new Vector3(0, 0, rotations[rand]);
        transform.eulerAngles = new Vector3(0, 0, Mathf.Round(transform.eulerAngles.z));

        if (PossibleRots > 1)
        {
            if (transform.eulerAngles.z == correctRotation[0] || transform.eulerAngles.z == correctRotation[1])
            {
                isPlaced = true;
                gameManager.GetComponent<GameManager2>().correctMove();
            }
        }
        else
        {
            if (transform.eulerAngles.z == correctRotation[0])
            {
                isPlaced = true;
                gameManager.GetComponent<GameManager2>().correctMove();
            }
        }
    }

    public void OnMouseDown()
    {
        transform.Rotate(0, 0, 90);
        transform.eulerAngles = new Vector3(0, 0, Mathf.Round(transform.eulerAngles.z));

        if (PossibleRots > 1)
        {
            if (transform.eulerAngles.z == correctRotation[0] || transform.eulerAngles.z == correctRotation[1] && isPlaced == false)
            {
                isPlaced = true;
                gameManager.GetComponent<GameManager2>().correctMove();
            }
            else if (isPlaced == true)
            {
                isPlaced = false;
                gameManager.GetComponent<GameManager2>().wrongMove(); 
            }
        }
        else
        {
            if (transform.eulerAngles.z == correctRotation[0] && isPlaced == false)
            {
                isPlaced = true;
                gameManager.GetComponent<GameManager2>().correctMove();
            }
            else if (isPlaced == true)
            {
                isPlaced = false;
                gameManager.GetComponent<GameManager2>().wrongMove();
            }
        }
    }
}
