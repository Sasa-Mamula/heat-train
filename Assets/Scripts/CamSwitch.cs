using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamSwitch : MonoBehaviour
{
    public GameObject MainCamera;
    public GameObject TurretCamera;
    

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("MainCam"))
        {
            MainCamera.SetActive(true);
            TurretCamera.SetActive(false);
        }

        if (Input.GetButtonDown("SecCam"))
        {
            MainCamera.SetActive(false);
            TurretCamera.SetActive(true);
        }

    }
}
