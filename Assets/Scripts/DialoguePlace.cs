using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialoguePlace : MonoBehaviour
{
  public Place[] places;


    [System.Serializable]
    public class Place
    {
        public Transform place;
        public string name;
    }
}
