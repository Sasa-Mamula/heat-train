using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class HoverOver2 : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public GameObject[] CharacterStats;
    public RectTransform[] backgroundBox;
    public GameObject turret;

    
    public void OnPointerEnter(PointerEventData eventData)
    {
        if (turret.GetComponent<Turret>().projectSpeed <= 89 && turret.GetComponent<Turret>().fireRate <= 8)
        {
            CharacterStats[0].SetActive(true);
            CharacterStats[1].SetActive(true);
            backgroundBox[0].LeanScale(Vector3.one, 0.5f).setEaseOutExpo();
            backgroundBox[1].LeanScale(Vector3.one, 0.5f).setEaseOutExpo();
        }
        else
        {
            CharacterStats[0].SetActive(false);
            CharacterStats[1].SetActive(false);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        CharacterStats[0].SetActive(false);
        CharacterStats[1].SetActive(false);
        backgroundBox[0].LeanScale(Vector3.zero, 0.5f).setEaseOutExpo();
        backgroundBox[0].LeanScale(Vector3.zero, 0.5f).setEaseOutExpo();
    }

    private void Start()
    {
        turret = GameObject.FindWithTag("Turret");
        backgroundBox[0].transform.transform.localScale = Vector3.zero;
        backgroundBox[1].transform.transform.localScale = Vector3.zero;
    }


}
