using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDamage : MonoBehaviour
{
    public int damage = 1;
    public int health = 200;
    public int currentHealth;
    Transform Train;
    int MoveSpeed = 2;
    int MinDist = 1;
    private GameObject upgradeTurret;
    private Animator animator;
    private GameObject player;
    // Timer to track collision time
    float _timeColliding;
    // Time before damage is taken, 1 second default
    public float timeThreshold = 1f;
    public GameObject bulletEffect;

    void Start()
    {
        currentHealth = health;
        Train = GameObject.FindGameObjectWithTag("Train").transform;
        upgradeTurret = GameObject.FindWithTag("Turret");
        animator = GetComponent<Animator>();
        player = GameObject.FindWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        if (Train != null)
        {
            transform.LookAt(Train);


            if (Vector3.Distance(transform.position, Train.position) >= MinDist)
            {

                transform.position += transform.forward * MoveSpeed * Time.deltaTime;
                animator.SetBool("Run", true);

            }
        }
        else
        {
           animator.SetBool("Run", false);
           Train = null;
        }

        StartCoroutine(die());
       
    }
    public void GiveDamage (float damage)
    {
        
            Train.GetComponent<Follower>().TakeDamage(damage);
      
    }
    IEnumerator die()
    {
       
        if (currentHealth <= 0)
        {

            animator.SetBool("Dead", true);
            yield return new WaitForSeconds(1);
            Destroy(this.gameObject);

        }

    }
    public void TakeDamage(int damage)
    {
        
        currentHealth -= damage;
        


    }
    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag == "Train" ) 
        {
            if (_timeColliding < timeThreshold)
            {
                _timeColliding += Time.deltaTime;
                animator.SetBool("Attack", true);


            }
            else
            {
                animator.SetBool("Attack", true);
                GiveDamage(damage);
                Train.GetComponent<Follower>().TakeSatisfy(0.5f);
                _timeColliding = 0f;
            }
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Bullet" )
        {
            Instantiate(bulletEffect, transform.position, transform.rotation);
           

            if (gameObject != null)
            {
                if (upgradeTurret.GetComponent<Turret>().fireRate == 5)
                {
                    
                    TakeDamage(5);
                    
                }
               
                

               if (upgradeTurret.GetComponent<Turret>().fireRate == 7)
               {
                   
                    TakeDamage(10);
                   
                }

                if (upgradeTurret.GetComponent<Turret>().fireRate == 9)
                {
                   
                    TakeDamage(20);
                   
                }
                
            }
        }
       


    }
  
}
