using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DistributionGame : MonoBehaviour
{
    public Slider[] Water;
    public TMP_Text[] currentAss;
    public TMP_Text[] currentAV;
    public TMP_Text[] minV;
    public TMP_Text[] maxV;
    public TMP_Text[] SliderV;
    public float[] minS;
    public float[] maxS;
    public GameObject[] distribution;
    public GameObject res;
    public GameObject Train;
    public Button[] confirm;
    AudioSource pay;
    // Start is called before the first frame update
    void Start()
    {
        pay = GetComponent<AudioSource>();
        Train = GameObject.FindGameObjectWithTag("Train");
        res =GameObject.Find("Humanoid-GEN");
        //Water canvas
        minS[0] = 100;
        maxS[0] = 10000;
        Water[0].minValue = minS[0];
        Water[0].maxValue = maxS[0];
        currentAss[0].text ="Currently Assigned: " + minS[0].ToString();
       

        //Oxygen canvas
        minS[1] = 0.1f;
        maxS[1] = 1;
        Water[1].minValue = minS[1];
        Water[1].maxValue = maxS[1];
        currentAss[1].text = "Currently Assigned: " + minS[1].ToString();
       

        //Food canvas
        minS[2] = 100;
        maxS[2] = 10000;
        Water[2].minValue = minS[2];
        Water[2].maxValue = maxS[2];
        currentAss[2].text = "Currently Assigned: " + minS[2].ToString();
        


    }

    // Update is called once per frame
    void Update()
    {
        //Water canvas
        minV[0].text = minS[0].ToString();
        maxV[0].text = maxS[0].ToString();
        SliderV[0].text = Water[0].value.ToString("F0");
        //Oxygen canvas
        minV[1].text = minS[1].ToString();
        maxV[1].text = maxS[1].ToString();
        SliderV[1].text = Water[1].value.ToString("0.00");
        //Food canvas
        minV[2].text = minS[2].ToString();
        maxV[2].text = maxS[2].ToString();
        SliderV[2].text = Water[2].value.ToString("F0");

        currentAV[0].text = "Currently Available: " + res.GetComponent<PlayerStats>().water.ToString("F0");
       
        currentAV[1].text = "Currently Available: " + res.GetComponent<PlayerStats>().oxygen.ToString("F0");

        currentAV[2].text = "Currently Available: " + res.GetComponent<PlayerStats>().food.ToString("F0");


        if (Input.GetKeyDown(KeyCode.E))
        {
            distribution[0].GetComponent<Canvas>().enabled = false;
            distribution[1].GetComponent<Canvas>().enabled = false;
            distribution[2].GetComponent<Canvas>().enabled = false;
        }
        
    }

    public void ConfirmWater()
    {
        //Water canvas
        float waterValue = Water[0].value;
        currentAss[0].text = "Currently Assigned: " + Water[0].value.ToString("F0");
        PlayerPrefs.SetFloat("WaterValue", waterValue);
        StartCoroutine(hide());
        res.GetComponent<PlayerStats>().water -= waterValue;
        Train.GetComponent<Follower>().GiveSatisfy(2);
        pay.Play();

        if (res.GetComponent<PlayerStats>().water <= 1)
        {
            confirm[0].interactable = false;
            Water[0].interactable = false;
        }
        
    }
    public void ConfirmOxygen()
    {
        //Water canvas
        float oxygenValue = Water[1].value;
        currentAss[1].text = "Currently Assigned: " + oxygenValue;
        PlayerPrefs.SetFloat("OxygenValue", oxygenValue);
        StartCoroutine(hide());
        res.GetComponent<PlayerStats>().oxygen -= oxygenValue;
        Train.GetComponent<Follower>().GiveSatisfy(2);
        pay.Play();
        if (res.GetComponent<PlayerStats>().oxygen <= 1)
        {
            confirm[1].interactable = false;
            Water[1].interactable = false;
        }
        
    }

    public void ConfirmFood()
    {
        //Water canvas
        float foodValue = Water[2].value;
        currentAss[2].text = "Currently Assigned: " + Water[2].value.ToString("F0");
        PlayerPrefs.SetFloat("FoodValue", foodValue);
        StartCoroutine(hide());
        res.GetComponent<PlayerStats>().food -= foodValue;
        Train.GetComponent<Follower>().GiveSatisfy(2);
        pay.Play();
        if (res.GetComponent<PlayerStats>().food <= 1)
        {
            confirm[2].interactable = false;
            Water[2].interactable = false;
        }
        
    }

    IEnumerator hide()
    {
        yield return new WaitForSeconds(0.5f);
        distribution[0].GetComponent<Canvas>().enabled = false;
        distribution[1].GetComponent<Canvas>().enabled = false;
        distribution[2].GetComponent<Canvas>().enabled = false;
    }


    public void WaterCanvas()
    {
        distribution[0].GetComponent<Canvas>().enabled = true;
        distribution[1].GetComponent<Canvas>().enabled = false;
        distribution[2].GetComponent<Canvas>().enabled = false;
    }

    public void OxygenCanvas()
    {
        distribution[1].GetComponent<Canvas>().enabled = true;
        distribution[0].GetComponent<Canvas>().enabled = false;
        distribution[2].GetComponent<Canvas>().enabled = false;
    }

    public void FoodCanvas()
    {
        distribution[2].GetComponent<Canvas>().enabled = true;
        distribution[0].GetComponent<Canvas>().enabled = false;
        distribution[1].GetComponent<Canvas>().enabled = false;
    }

    
    
}
