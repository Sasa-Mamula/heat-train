using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CharShopCanvas : MonoBehaviour
{
    public GameObject Charcanvas;
    private bool player;
    private bool shopEnabled;
    public TMP_Text text;

    // Start is called before the first frame update
    void Start()
    {
        text.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            shopEnabled = !shopEnabled;
            
        }

        if (player && shopEnabled)
        {
            Charcanvas.GetComponent<Canvas>().enabled = true;

        }
        else
        {
            Charcanvas.GetComponent<Canvas>().enabled = false;

        }

        if (Charcanvas.GetComponent<Canvas>().enabled == true)
        {
            text.gameObject.SetActive(false);
        }


    }

    

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            text.gameObject.SetActive(true);

            player = true;


        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            text.gameObject.SetActive(false);
            Charcanvas.GetComponent<Canvas>().enabled = false;
            player = false;
            
        }
    }
}


