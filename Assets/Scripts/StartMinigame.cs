using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartMinigame : MonoBehaviour
{
    public GameObject startMinigame;

   
    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            startMinigame.GetComponent<Canvas>().enabled = true;
            this.GetComponent<BoxCollider>().enabled = false ;

        }

    }
}
