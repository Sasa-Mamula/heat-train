using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerStats : MonoBehaviour
{
    
    public float metal = 40000;
    public float water = 500;
    public float oxygen = 500;
    public float food = 500;
    public Slider HealthBar;
    public TMP_Text metalText;
    
    private void Update()
    {
     
        metalText.text = metal.ToString("F0");
    }

}
