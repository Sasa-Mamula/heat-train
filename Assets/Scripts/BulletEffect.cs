using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletEffect : MonoBehaviour
{
    public GameObject impactEffect;

    private void Update()
    {
        Instantiate(impactEffect, transform.position, transform.rotation);
    }
}
