using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class WaveSpawner : MonoBehaviour
{
    public enum SpawnState { SPAWNING, WAITING, COUNTING, STOP, UPDATE };

    [System.Serializable] // potrebno da se moze klasa uredjivati u unity-u
    public class Wave
    {
        public string name; //ime wave-a
        public Transform enemy;
        public int count;
        public float rate;
    }

    public Wave[] waves;
    public int nextWave = 0;

    public Transform[] spawnPoints;

    public float timeBetweenWaves = 3f;
    public float waveCountdown;
    public Slider Health;
    private float searchCountdown = 1f;
    public TMP_Text completeWave;
    GameObject Train;
    private GameObject upgradeTurret;




    public SpawnState state = SpawnState.COUNTING;
    private void Start()
    {
        Train = GameObject.FindGameObjectWithTag("Train");
        this.enabled = false;
        if (spawnPoints.Length == 0)
        {
            Debug.LogError("No spawn points refrenced");
        }
        upgradeTurret = GameObject.FindWithTag("Turret");
        waveCountdown = timeBetweenWaves;
        AnimateTextColor();
        completeWave.gameObject.SetActive(false);
    }

    private void Update()
    {
        if (upgradeTurret.GetComponent<Turret>().fireRate == 5 && nextWave == 1)
        {
            AnimateTextColor();
            completeWave.gameObject.SetActive(true);
            completeWave.text = "Next wave requires turret level 2 !";
            waveCountdown += Time.deltaTime;

        }


        if (upgradeTurret.GetComponent<Turret>().fireRate == 7 && nextWave == 2)
        {
            AnimateTextColor();
            completeWave.gameObject.SetActive(true);
            completeWave.text = "Next wave requires turret level 3 !";

            waveCountdown += Time.deltaTime;
        }
        

        //pravljenje waiting methode
        if (state == SpawnState.WAITING)
        {
            completeWave.gameObject.SetActive(false);
            AnimateTextColor();
            //check if enemy is still alive
            if (EnemyAlive())
            {

                return;
            }
            else
            {

                //Begin a new wave
                WaveCompleted();
            }

        }

        if (state == SpawnState.STOP)
        {
            waveCountdown += Time.deltaTime;


        }

        if (waveCountdown <= 0)
        {
            if (state != SpawnState.SPAWNING)
            {
                //start spawning wave
                StartCoroutine(SpawnWave(waves[nextWave]));

            }
            else completeWave.gameObject.SetActive(false);

        }
        else
        {

            waveCountdown -= Time.deltaTime;
        }
    }
    void WaveCompleted()
    {
        Debug.Log("Wave Completed");

        state = SpawnState.COUNTING;
        waveCountdown = timeBetweenWaves;


        //ukoliko se zavrse tri wave-a da ne trai spawnanje 4 kojeg nema
        if (nextWave + 1 > waves.Length - 1)
        {      
            Health.gameObject.SetActive(false);
            AnimateTextColor();
            completeWave.gameObject.SetActive(true);
            completeWave.text = "You won!";
            AnimateTextColor();
            Train.GetComponent<Follower>().GiveSatisfy(30);
            state = SpawnState.STOP;
            Destroy(completeWave.gameObject, 3f);

        }else nextWave++;

        /*
        if (nextWave == 1)
         {
             completeWave.gameObject.SetActive(true);
             AnimateTextColor();
             completeWave.text = "You should upgrade turret to level 2!";

         }
        if (nextWave == 2)
        {
            completeWave.gameObject.SetActive(true);
            AnimateTextColor();
            completeWave.text = "You should upgrade turret to level 3!";
            nextWave++;
        }
        */
        

    }

    bool EnemyAlive()
    {
        searchCountdown -= Time.deltaTime;
        if (searchCountdown <= 0f)
        {
            searchCountdown = 1f;
            if (GameObject.FindWithTag("Enemy") == null)
            {
                return false;
            }
        }


        return true;
    }


    //zelimo da mozemo pricekati nekoliko sekundi da moze nastaviti spownati
    IEnumerator SpawnWave(Wave _wave)
    {
        
        state = SpawnState.SPAWNING;

        for (int i = 0; i < _wave.count; i++)
        {
            _wave.enemy.gameObject.tag = "Enemy";
            SpawnEnemy(_wave.enemy);



            yield return new WaitForSeconds(1f / _wave.rate);

        }

        //?ekanje da enemy-e turret unisti
        state = SpawnState.WAITING;

        yield break;
    }

    void SpawnEnemy(Transform _enemy)
    {
        

        Transform _sp = spawnPoints[Random.Range(0, spawnPoints.Length)];
        Instantiate(_enemy, _sp.position, _sp.rotation);
        _enemy.gameObject.tag = "Enemy";

    }

    void AnimateTextColor()
    {
        LeanTween.textAlpha(completeWave.rectTransform, 0, 0);
        LeanTween.textAlpha(completeWave.rectTransform, 1, 0.5f);
        LeanTween.textAlpha(completeWave.rectTransform, 0, 0);
        LeanTween.textAlpha(completeWave.rectTransform, 1, 0.5f);
    }


}
