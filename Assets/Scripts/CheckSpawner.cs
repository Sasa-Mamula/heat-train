using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CheckSpawner : MonoBehaviour
{
    
    public Text arrived;
    public Slider Health;
    
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
         StartCoroutine(EnableScript());
        }
    }
 
    IEnumerator EnableScript()
    {
        yield return new WaitForSeconds(5);
        GameObject.FindGameObjectWithTag("Spawner").GetComponent<WaveSpawner>().enabled = true;
        AnimateTextColor();
        arrived.gameObject.SetActive(true);
        Health.gameObject.SetActive(true);
        yield return new WaitForSeconds(5);
        arrived.gameObject.SetActive(false);
        
    }
    

    private void OnTriggerExit(Collider other)
    {
        arrived.gameObject.SetActive(false);
        this.gameObject.GetComponent<BoxCollider>().enabled = false;
    }
    void AnimateTextColor()
    {
        LeanTween.textAlpha(arrived.rectTransform, 0, 0);
        LeanTween.textAlpha(arrived.rectTransform, 1, 0.5f);
        LeanTween.textAlpha(arrived.rectTransform, 0, 0);
        LeanTween.textAlpha(arrived.rectTransform, 1, 0.5f);
    }
}
